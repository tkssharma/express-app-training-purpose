var createError = require('http-errors');
var express = require('express');
// express as framework 
var path = require('path');
//---------------------------------//
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//------------------------------------//
// routes 
// import routes from different files 

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
// create express instance 
var app = express();
// app is express instance 
// app.use()
// app.set('port' , 6000);
// ejs as templating egine 
// views dir & engine name 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// .ejs files only in views folder 


// morgan is a logger log your path 
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// node js core // http server /
// routes // middleware 
// read daat from body || read data from cookies 
app.use(express.static(path.join(__dirname, 'public')));
//-----------------------------------------------------------//
// parse application/x-www-form-urlencoded
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
//--------------------------------------------------------------//
// app.use() // 
// middleware we have created 
// app instance 
// every request will execute this code 
// app.use()

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
