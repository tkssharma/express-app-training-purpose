var express = require('express');
// require('x') => cached => require('x');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  // SSR
  // template rendering 
  // index.ejs by passing title there 
  res.render('index', { title: 'Express' });
});
router.post('/', function(req, res, next) {
  // sending json object 
  // res.json()
  // res.render();  // template
  // res.json()   // json
  // res.send()   / html text back 
  res.json(req.body);
});

module.exports = router;


